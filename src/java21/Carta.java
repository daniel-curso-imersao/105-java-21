package java21;

public class Carta {
	private Naipe naipe;
	private Tipo tipo;
	private int valor;

	public Carta(Naipe naipe, Tipo tipo) {
		this.naipe = naipe;
		this.tipo = tipo;
		this.valor = tipo.getValorInicial();
	}

	public Naipe getNaipe() {
		return naipe;
	}

	public Tipo getTipo() {
		return tipo;
	}
	
	public int getValor() {
		return valor;
	}

	public void mudarValorAs() {
		if (this.tipo == Tipo.AS && this.valor == 11) {
			this.valor = 1;
		}
	}

	@Override
	public String toString() {
		return tipo + " " + naipe;
	}

}
