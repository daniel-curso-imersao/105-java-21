package java21;
import java.util.ArrayList;
import java.util.List;


public class Jogador {
	private int numero;
	private ArrayList<Carta> mesa;
	private StatusJogo statusJogo;
	private Baralho baralho;

	public Jogador(int numero) {
		this.numero = numero;
		this.mesa = new ArrayList<>();	
		this.baralho = new Baralho();
		this.statusJogo = StatusJogo.NAO_INICIADO;
	}
	
	public int getNumero() {
		return numero;
	}

	public StatusJogo getStatusJogo() {
		return statusJogo;
	}

	public List<Carta> getMesa() {
		return mesa;
	}
	
	public void puxarCarta() {
		Carta cartaPuxada = baralho.puxarCarta(1).get(0);
		adicionarCarta(cartaPuxada);
	}

	public void adicionarCarta(Carta carta) {
		mesa.add(carta);
		if (getTotalPontos() == 21) {
			this.statusJogo = StatusJogo.GANHOU;
		} else if (getTotalPontos() > 21) {
			recalcular();
			if (getTotalPontos() > 21) {
				this.statusJogo = StatusJogo.PERDEU;
			}
		}
	}
	
	public void iniciarJogo() {
		for (Carta carta : baralho.puxarCarta(2)) {
			adicionarCarta(carta);
		}		
		statusJogo = StatusJogo.JOGANDO;
	}
	
	public void setJogo(StatusJogo statusJogo) {
		this.statusJogo = statusJogo;
	}

	public void parar() {
		this.statusJogo = StatusJogo.PAROU;
	}

	public int getTotalPontos() {
		int totalPontos = 0;
		for (Carta carta : mesa) {
			totalPontos += carta.getValor();
		}
		return totalPontos;
	}

	private void recalcular() {
		Carta c = mesa.stream().filter(ca -> ca.getTipo() == Tipo.AS && ca.getValor() == 11)
				.findFirst().orElse(null);
		if (c != null) {
			c.mudarValorAs();
		}
	}

}
