package java21;
import java.text.MessageFormat;

public class Visor {

	public void imprimirMesa(String msg, Jogador jogador, boolean imprimirPontos) {

		StringBuilder sb = new StringBuilder();

		// Formatar valores das cartas na mensagem
		for (Carta carta : jogador.getMesa()) {
			String naipe = carta.getNaipe().toString();
			String valorCarta = carta.getTipo().getDescricao();

			String cartaMensagem = MessageFormat.format("[{0} de {1}]", valorCarta, naipe);

			if (!Util.isUltimoDaLista(jogador.getMesa(), carta)) {
				sb.append(cartaMensagem + ", ");
			} else {
				sb.append(cartaMensagem);
			}
		}

		String msgCartas = MessageFormat.format("{0}: {1}", msg, sb.toString());

		System.out.println(msgCartas);
		if (imprimirPontos)
			System.out.println("Total de pontos: " + jogador.getTotalPontos());
	}
	
	public void imprimirInicioRodada(int rodada, Jogador jogador) {
		imprimirMensagem(MessageFormat.format("RODADA {0} - JOGADOR {1} --------------------------", rodada, jogador.getNumero()));
	}
	
	public void imprimirPerguntaPuxarCarta(Jogador jogador) {
		String msg = MessageFormat.format("JOGADOR {0}> Deseja continuar(s/n)?", jogador.getNumero());
		System.out.println(msg);		
	}
	
	public void imprimirMensagem(String msg) {
		System.out.println(msg);
	}

	public void imprimirFimDeJogo(Jogador jogador1, Jogador jogador2) {
		System.out.println("--------FIM DE JOGO-----------------");
		if (jogador1.getStatusJogo() == StatusJogo.GANHOU) {
			System.out.println("Jogador 1 vitorioso.");
		} 
		if (jogador1.getStatusJogo() == StatusJogo.PERDEU) {
			System.out.println("Jogador 1 perdedor.");
		}
		
		if (jogador2.getStatusJogo() == StatusJogo.GANHOU) {
			System.out.println("Jogador 2 vitorioso.");
		}
		if (jogador2.getStatusJogo() == StatusJogo.PERDEU) {
			System.out.println("Jogador 2 perdedor.");
		}
		
		if (jogador1.getStatusJogo() == StatusJogo.EMPATOU) {
			System.out.println("A partida empatou.");
		}
	}

}
