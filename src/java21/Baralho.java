package java21;
import java.util.ArrayList;
import java.util.List;

public class Baralho {
	private List<Carta> cartas;
	private int cartasRestantes;
	
	public Baralho() {
		cartas = new ArrayList<>();

		for (Tipo tipo : Tipo.values()) {
			for (Naipe naipe : Naipe.values()) {
				Carta carta = new Carta(naipe, tipo);
				cartas.add(carta);
			}
		}

		this.cartasRestantes = cartas.size();
	}

	public List<Carta> puxarCarta(int qtd) {
		ArrayList<Carta> listaCartas = new ArrayList<>();

		for (int i = 1; i <= qtd; i++) {
			int id = (int) Math.floor(Math.random() * cartas.size());
			Carta carta = cartas.get(id);
			listaCartas.add(carta);
			cartas.remove(id);
		}
		this.cartasRestantes = cartas.size();
		return listaCartas;
	}
	
	public int getCartasRestantes() {
		return this.cartasRestantes;
	}

}