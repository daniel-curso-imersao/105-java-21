package java21;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Util {
	public static String separador = System.getProperty("line.separator");
	
	public static String formatarReais(double valor) {
		return String.valueOf(valor).replace(".", ",");
	}
	
	public static <T> boolean isPrimeiroItem(List<T> lista, T elemento) {
		if (lista.size() > 0) {
			return lista.get(0).equals(elemento);
		}
		return false;		
	}
	
	public static <T> boolean isUltimoDaLista(List<T> lista, T elemento) {
		if (lista.size() > 0) {					
			return lista.get(lista.size() - 1).equals(elemento);
		}
		return false;
	}
	
	public static void inicializarArquivoExercicio(Path path, byte[] conteudo) throws IOException {
		Files.deleteIfExists(path);
		Files.createFile(path);
		Files.write(path, conteudo, StandardOpenOption.APPEND);
	}
}
