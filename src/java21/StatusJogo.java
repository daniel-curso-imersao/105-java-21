package java21;

public enum StatusJogo {
	NAO_INICIADO,
	JOGANDO,
	GANHOU,
	PERDEU,
	PAROU,
	EMPATOU
}
