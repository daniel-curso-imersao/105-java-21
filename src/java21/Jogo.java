package java21;
import java.util.Scanner;

public class Jogo {

	public static void main(String[] args) {
		String resposta = "s";
		boolean isJogoFinalizado = false;
		int rodada = 0;
		Jogador jogador1 = new Jogador(1);
		Jogador jogador2 = new Jogador(2);
		Jogador jogadorTurno = jogador1;
		Visor visor = new Visor();

		try (Scanner scanner = new Scanner(System.in)) {
			while (!isJogoFinalizado) {

				visor.imprimirInicioRodada(rodada, jogadorTurno);

				if (jogadorTurno.getStatusJogo() == StatusJogo.NAO_INICIADO) {
					jogadorTurno.iniciarJogo();
				}

				if (jogadorTurno.getStatusJogo() == StatusJogo.GANHOU) {
					isJogoFinalizado = true;
					break;
				}

				visor.imprimirMesa("Cartas na mesa", jogadorTurno, true);
				visor.imprimirPerguntaPuxarCarta(jogadorTurno);				
				resposta = scanner.nextLine();

				// Validação de outros caracteres
				if (!resposta.toLowerCase().equals("s") && !resposta.toLowerCase().equals("n")) {
					visor.imprimirMensagem("Digite APENAS 's' ou 'n'!!");
					continue;
				}

				if (resposta.toLowerCase().equals("s")) {
					jogadorTurno.puxarCarta();
					visor.imprimirMesa("Cartas na mesa", jogadorTurno, true);
				} else {
					jogadorTurno.parar();
				}

				// Validar se houve alguém que ganhou/perdeu ou se os ambos pararam
				if (jogador1.getStatusJogo() == StatusJogo.GANHOU ||
					jogador2.getStatusJogo() == StatusJogo.GANHOU ||
					jogador1.getStatusJogo() == StatusJogo.PERDEU ||					
					jogador2.getStatusJogo() == StatusJogo.PERDEU || 
					(jogador1.getStatusJogo() == StatusJogo.PAROU && 
						jogador2.getStatusJogo() == StatusJogo.PAROU))  {
					// Comparar placares e finalizar o jogo
					compararPlacares(jogador1, jogador2);
					isJogoFinalizado = true;
				}

				// Trocar turno
				if (jogadorTurno == jogador1 && (jogador2.getStatusJogo() == StatusJogo.JOGANDO
						|| jogador2.getStatusJogo() == StatusJogo.NAO_INICIADO)) {
					jogadorTurno = jogador2;
				} else if (jogadorTurno == jogador2 && (jogador1.getStatusJogo() == StatusJogo.JOGANDO
						|| jogador1.getStatusJogo() == StatusJogo.NAO_INICIADO)) {
					jogadorTurno = jogador1;
				}

				rodada++;
			}

			visor.imprimirFimDeJogo(jogador1, jogador2);
		}
	}

	private static void compararPlacares(Jogador jogador1, Jogador jogador2) {
		int jogador1Pontos = jogador1.getTotalPontos();
		int jogador2Pontos = jogador2.getTotalPontos();

		if (jogador1Pontos > 21) {
			jogador1.setJogo(StatusJogo.PERDEU);
			jogador2.setJogo(StatusJogo.GANHOU);
			return;
		}
		if (jogador2Pontos > 21) {
			jogador2.setJogo(StatusJogo.PERDEU);
			jogador1.setJogo(StatusJogo.GANHOU);
			return;
		}
		if (jogador1Pontos == 21) {
			jogador1.setJogo(StatusJogo.GANHOU);
			jogador2.setJogo(StatusJogo.PERDEU);
			return;
		}
		if (jogador2Pontos == 21) {
			jogador2.setJogo(StatusJogo.GANHOU);
			jogador1.setJogo(StatusJogo.PERDEU);
			return;
		}
		
		if (jogador1Pontos == jogador2Pontos) {
			jogador1.setJogo(StatusJogo.EMPATOU);
			jogador2.setJogo(StatusJogo.EMPATOU);
			return;
		}

		if (jogador1Pontos > jogador2Pontos) {
			jogador1.setJogo(StatusJogo.GANHOU);
			jogador2.setJogo(StatusJogo.PERDEU);
		} else {
			jogador2.setJogo(StatusJogo.GANHOU);
			jogador1.setJogo(StatusJogo.PERDEU);
		}
	}
}
